#!/bin/sh

[ "x$1" = "x" ] && VER="$(uname -r)" || VER="$1"
kexec --reuse-cmdline --initrd="/boot/initramfs-$VER.img" -l "/boot/vmlinuz-$VER" && systemctl kexec
