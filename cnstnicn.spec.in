Name:           cnstnicn
Version:        @VERSION@
Release:        1
Summary:        Gather data for Consistent NIC naming
License:        MIT

Source0:        %{name}-%{version}.tar.xz

BuildArch:      noarch
BuildRequires:  systemd
Requires:       curl kexec-tools

%description
Data gathering scripts for the Consistent NIC naming effort.
 - Downloads several RHEL 8 and RHEL 9 kernels to be tested.
 - Installs the kernels one by one and reboots into them with kexec.
 - Gathers the relevant sysfs attributes of NICs.
 - Submits the results to a remote syslog server.

Install it on a recent RHEL 9 Ystream compose.
Start the process with 'start-cnstnicn.sh'.

WARNING:
It will reboot (kexec) the system many times. It will most likely
remove your installed kernels and leave you with the tested ones.

%prep
%autosetup

%build

%install
%make_install

%files
%{_bindir}/cnstnicn-next-kernel.sh
%{_bindir}/cnstnicn-submit.sh
%{_bindir}/download-kernels.sh
%{_bindir}/kexec.sh
%{_bindir}/start-cnstnicn.sh
%{_unitdir}/cnstnicn-next-kernel.service
%{_unitdir}/cnstnicn-submit.service
%{_datadir}/%{name}
%{_sharedstatedir}/%{name}

%changelog
* @DATE@ Michal Schmidt <mschmidt@redhat.com> - @VERSION@-1
- generated
